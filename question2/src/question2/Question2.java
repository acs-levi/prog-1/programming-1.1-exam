package question2;

public class Question2 {
    public static void main(String[] args) {


        Computer computer1 = new Computer();
        Computer computer2 = new Computer("Dell", 8); // This machine has 8Gb of RAM

          computer1.setBrand("HP");
            computer1.setRam(1);

        System.out.println("Is computer1 ancient? " + computer1.isAncient());  //true
        System.out.println("Is computer2 ancient? " + computer2.isAncient());  //false

        System.out.println("This is the String representation of computer2: " + computer2);
    }
}
